/**
 * Created by Seebo on 8/10/2014.
 */
package {
import dragonBones.Armature;
import dragonBones.animation.WorldClock;
import dragonBones.events.AnimationEvent;
import dragonBones.factorys.StarlingFactory;

import flash.events.Event;

import starling.core.Starling;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;

public class ArmaturePlayExamples extends Sprite {

    [Embed(source="assets/level_13_path.png", mimeType="application/octet-stream")]

    public static const ResourcesData:Class;

    private var _factory:StarlingFactory;

    private var _buttonPlay:Button;
    private var _buttonPlayReverse:Button;
    private var _buttonOneAnimationInReverse:Button;
    private var _figureArmature:Armature;
    private var _figureArmatureClip:Sprite;

    private var _pathArmature:Armature;
    private var _pathArmatureClip:Sprite;

    public function ArmaturePlayExamples() {
        _factory = new StarlingFactory();
        _factory.addEventListener(Event.COMPLETE, textureCompleteHandler);
        _factory.parseData(new ResourcesData());
        initButtons();

    }

    private function textureCompleteHandler(e:Event):void {
        _figureArmature = _factory.buildArmature("fireman_anim");
        _figureArmatureClip = _figureArmature.display as Sprite;
        _figureArmatureClip.x = 80;
        _figureArmatureClip.y = 660;
        _figureArmature.animation.gotoAndPlay("fireman_idle");
        addChild(_figureArmatureClip);
        WorldClock.clock.add(_figureArmature);

        _pathArmature = _factory.buildArmature("level13/graphics/path_1");
        _pathArmatureClip = _pathArmature.display as Sprite;
        _pathArmature.getBone("figure").display.visible = false;
        addChild(_pathArmatureClip);
        WorldClock.clock.add(_pathArmature);
        _pathArmatureClip.x = -500;
        _pathArmatureClip.y = -250;

        addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);

    }

    private function onEnterFrame(_e:EnterFrameEvent):void {
        WorldClock.clock.advanceTime(-1);
    }


    private function initButtons():void {
        _buttonPlay = new Button("Play Sequence Of Animations Normally");
        _buttonPlay.clickEvent.set(onPlayButtonClick);
        _buttonPlay.addUI(Starling.current.nativeOverlay, 50, 50);

        _buttonPlayReverse = new Button("Play Sequence of Animations In Reverse");
        _buttonPlayReverse.clickEvent.set(onPlayReverseButtonClick);
        _buttonPlayReverse.addUI(Starling.current.nativeOverlay, 50, 100);

        _buttonOneAnimationInReverse = new Button("Play One Animation In Reverse");
        _buttonOneAnimationInReverse.clickEvent.set(onPlayOneReverseButtonClick);
        _buttonOneAnimationInReverse.addUI(Starling.current.nativeOverlay, 50, 150);

    }

    //------------------------- PLAY--------------------------


    private function onPlayButtonClick():void {
        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onSlideAnimationLoopComplete);
        _figureArmature.animation.gotoAndPlay("fireman_slide");

        _pathArmature.getBone("figure").display.dispose();
        _pathArmature.getBone("figure").display = _figureArmature.display;
        _pathArmature.animation.gotoAndPlay("slide");
        _pathArmature.getBone("figure").display.visible = true;
    }

    private function onSlideAnimationLoopComplete(event:AnimationEvent):void {
        _pathArmature.removeEventListener(AnimationEvent.LOOP_COMPLETE, onSlideAnimationLoopComplete);
        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onStartDrawingAnimationLoopComplete);
        _pathArmature.animation.stop();
        _pathArmature.animation.gotoAndPlay("start_drawing");
    }

    private function onStartDrawingAnimationLoopComplete(event:AnimationEvent):void {

        _pathArmature.removeEventListener(AnimationEvent.LOOP_COMPLETE, onStartDrawingAnimationLoopComplete);
        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onEndDrawingAnimationLoopComplete);
        _pathArmature.animation.stop();
        _pathArmature.animation.gotoAndPlay("end_drawing");
    }

    private function onEndDrawingAnimationLoopComplete(event:AnimationEvent):void {
        _pathArmature.removeEventListener(AnimationEvent.LOOP_COMPLETE, onEndDrawingAnimationLoopComplete);
        _pathArmature.animation.stop();
        _figureArmature.animation.gotoAndPlay("fireman_place_down");
    }


    //----------------------SEQUENCE REVERSE-------------------
    private function onPlayReverseButtonClick():void {
        _figureArmature.animation.gotoAndPlay("fireman_slide").setTimeScale(-1);

        _pathArmature.getBone("figure").display.dispose();
        _pathArmature.getBone("figure").display = _figureArmature.display;

        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onEndDrawingReverseLoopComplete);

        _figureArmature.animation.gotoAndPlay("fireman_slide").setTimeScale(-1);
        _pathArmature.animation.gotoAndPlay("end_drawing").setTimeScale(-1);

        _pathArmature.getBone("figure").display.visible = true;
    }

    private function onEndDrawingReverseLoopComplete(event:AnimationEvent):void {
        _pathArmature.removeEventListener(AnimationEvent.LOOP_COMPLETE, onEndDrawingReverseLoopComplete);
        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onStartDrawingReverseLoopComplete);
        _pathArmature.animation.stop();
        _pathArmature.animation.gotoAndPlay("start_drawing").setTimeScale(-1);
    }

    private function onStartDrawingReverseLoopComplete(event:AnimationEvent):void {

        _pathArmature.removeEventListener(AnimationEvent.LOOP_COMPLETE, onStartDrawingReverseLoopComplete);
        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onslideDrawingReverseLoopComplete);
        _pathArmature.animation.stop();
        _pathArmature.animation.gotoAndPlay("slide").setTimeScale(-1);
    }

    private function onslideDrawingReverseLoopComplete(event:AnimationEvent):void {

        _pathArmature.removeEventListener(AnimationEvent.LOOP_COMPLETE, onslideDrawingReverseLoopComplete);
        _pathArmature.animation.stop();
        _figureArmature.animation.gotoAndPlay("fireman_place_down");
    }

    //----------------------ONE ANIMATION IN REVERSE-------------------
    private function onPlayOneReverseButtonClick():void {
        _pathArmature.addEventListener(AnimationEvent.LOOP_COMPLETE, onPlayOnReverseLoopComplete);

        _figureArmature.animation.gotoAndPlay("fireman_slide").setTimeScale(-1);

        _pathArmature.getBone("figure").display.dispose();
        _pathArmature.getBone("figure").display = _figureArmature.display;

        _figureArmature.animation.gotoAndPlay("fireman_slide").setTimeScale(-1);
        _pathArmature.animation.gotoAndPlay("start_drawing").setTimeScale(-1);

        _pathArmature.getBone("figure").display.visible = true;

    }

    private function onPlayOnReverseLoopComplete(event:AnimationEvent):void {
        _pathArmature.animation.stop();
    }


}


}
