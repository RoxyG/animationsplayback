/**
 * Created by idoran on 5/12/14.
 */
package {
	/**
	 * Represent an event that allow single listener.
	 */
	public class SingleEvent
	{
		private var _handler : Function;

		public function SingleEvent()
		{
		}

		/**
		 * Set the handler to be called.
		 * @param handler Function to be called when the event invokes.
		 */
		public function set(handler : Function) : void
		{
			_handler = handler;
		}

		/**
		 * Remove the handler.
		 */
		public function remove() : void
		{
			_handler = null;
		}

		/**
		 * Invoke the event.
		 * @param args Arguments to pass to the handler.
		 */
		public function invoke(... args) : void
		{
			if (_handler != null)
			{
				_handler.apply(null, args);
			}
		}
	}
}
