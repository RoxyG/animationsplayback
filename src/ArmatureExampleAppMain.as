package {

import dragonBones.animation.WorldClock;

import flash.display.Sprite;
import flash.geom.Rectangle;
import flash.text.TextField;

import starling.core.Starling;
import starling.events.EnterFrameEvent;
import starling.events.Event;

public class ArmatureExampleAppMain extends Sprite {

    private var _starling:Starling;

    public function ArmatureExampleAppMain() {

        if (stage) {
            init();
        }
        else {
            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        }
    }

    private function init():void {
        _starling = new Starling(ArmaturePlayExamples, stage, new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight));
        _starling.showStats = true;
        _starling.start();
    }

    private function onAddedToStage(event:Event):void {
        removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

        init();
    }
}
}
