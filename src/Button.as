/**
 * Created by Seebo on 8/10/2014.
 */
package {

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class Button {

    private var _ui : Sprite;
    private var _button : TextField;
    private var _clickEvent : SingleEvent = new SingleEvent();

    public function Button(text : String) {

        _ui = new Sprite();

        _button = new TextField();
        _button.text = text;
        _button.autoSize = TextFieldAutoSize.LEFT;
        _button.addEventListener(MouseEvent.CLICK, onButtonClick);
        _ui.addChild(_button);
    }

    private function onButtonClick(event : MouseEvent) : void
    {
        _clickEvent.invoke();
    }

    public function addUI(container : DisplayObjectContainer, x : Number, y : Number) : void
    {
        _ui.x = x;
        _ui.y = y;
        container.addChild(_ui);
    }

    public function get clickEvent() : SingleEvent
    {
        return _clickEvent;
    }
}
}
